* emacs translation resources
This is a collection of things in emacs that are handy for translating, and info and discussion about what else might be done.

Feel free to contribute information if you know more. Open a PR or email me a patch: https://codeberg.org/martianh.

** translation packages

*** translate mode

https://github.com/rayw000/translate-mode/

- paragraph-based
- uses =master=
- two file model
- works with a pair of buffers
- new, not a lot of features, but actively developed

*** org-translate

https://elpa.gnu.org/packages/org-translate.html

- org-based
- features segmentation (auto in source, manual in target)
- single file source/target/glossary
- author plans to do a full re-write as pure text mode and using a standard glossary format (TMX).
- not really actively developed, but pretty well done.

*** emacs-cat

- really only just enables highlighting sentence in other buffer and moving the highlight without switching buffers.

** dictionaries

*** emacs-leo

https://github.com/mtenders/emacs-leo
(updated version
https://codeberg.org/martianh/emacs-leo)

- online
- interface to german focused online dictionaries

*** wordreference

https://codeberg.org/martianh/wordreference.el

- online
- interface to romance language focused dictionaries

*** reverso.el

https://github.com/SqrtMinusOne/reverso.el

- online
- includes translation, grammar, bilingual concordances, synonyms search.

*** helm-dictionary

https://github.com/emacs-helm/helm-dictionary

- offline
- works with wiktionary dicts

*** sdcv-mode

 https://repo.or.cz/sdcv.el.git

- offline
- star dict dictionaries
- old and ugly (uses =outline-mode= but there are high-quality offline dictionaries available for =sdcv=, such as the Oxford English Dictionary, Le Littré Dictionnaire de la langue française, etc.

*** wn.el

https://codeberg.org/martianh/wn.el

- worknik API
- api key (free)
- defintions, etymology, related words, examples
- bi-gram phrases

*** etym.el

https://codeberg.org/martianh/etym.el

- online
- scrapes https://etymonline.com
- quality etymological data
- bare bones display still

** thesauruses
*** mw-thesaurus

https://github.com/agzam/mw-thesaurus.el

- merriam webster, open/free (non-commercial) API.
- results formatted with org.

*** helm-wordnet

https://github.com/raghavgautam/helm-wordnet

*** synosaurus

https://github.com/hpdeifel/synosaurus

- uses wordnet (offline) and opendictionary (online) by default, can add others.
- displays results as a popup, or ido or minibuffer.

*** wikionary-bro

https://github.com/agzam/wiktionary-bro.el

- online
- pretty rendered shr views
- foldable results like org tree
- open/free data from wikionary.

*** powerthesaurus

* machine translation

** go-translate

https://github.com/lorniu/go-translate

- translation framework that supports various backends like google, bing, deepl.

** google-translate

** lingva.el

https://codeberg.org/martianh/lingva.el

- access google translations via lingva.ml instances, without google tracking.

** txl, deepl

https://github.com/tmalsburg/txl.el

- requires API key, subscribers only
- no longer offers a free tier for API access.

** ob-translate

https://github.com/krisajenkins/ob-translate

- translate text using org blocks
- not updated in a long time

** babel

https://github.com/juergenhoetzel/babel

* handy helpers

** hl-sentence

https://github.com/milkypostman/hl-sentence

- highlight current sentence

** hl-checkerboard

https://codeberg.org/martianh/hl-checkerboard

- highlight every second sentence or clause

** centered-cursor-mode

https://github.com/andre-r/centered-cursor-mode.el

** scroll-all-mode

- scrolls all (not just 2!) windows in a frame.

* other handy packages

** org-remark

https://github.com/nobiot/org-remark

- annotations in separate org buffer/file.

** annotate.el

 https://github.com/bastibe/annotate.el/

- in-buffer annotations as overlays
- doesn't play with =variable-pitch-mode=
- suited only for brief comments

** guess-language.el

https://github.com/tmalsburg/guess-language.el

** critic markup (=cm-mode=)

https://github.com/joostkremers/criticmarkup-emacs

- track-changes functionality.
- not recently updated but seems to work

** emacs-sentence-navigation

https://github.com/noctuid/emacs-sentence-navigation

- regexps for skipping abbreviations etc. when navigating (or highlighting) by sentence.

* desirable but non-existent features

** segmentation

- using CAT parsers for sophisticated segmentation/sentence-end functionality (break at normal sentence ending periods, but not at "Mr." or "etc.,")
   - "Once the text is extracted, it needs to be segmented. Basic "no" segmentation usually means paragraph based segmentation. Paragraphs are defined differently depending on the original format (1, or 2 line breaks for a text file, a block tag for XML-based formats, etc.). // Fine-grained segmentation is obtained by using a set of native language based regex that includes break rules and no-break rules. A simple example is break after a "period followed by a space" but don't break after "Mr. " for English." (from the discussion above)

- might it not be possible to convert OmegaT's Java segmentation regexes into Elisp, then plug them in to our sentence-end or segmentation variables?
   - the regexes are in pairs: before poss break and after poss break.
   - =segment.el= has almost achieved this now.

- OmegaT's segmentation info is in =/OmegaT_ver.sion.num_Source/src/org/omegat/core/segmentation/=
- the default rules are stored as XML =/OmegaT_ver.sion.num_Source/src/org/omegat/core/segmentation/defaultRules.srx=.
- Okapi's segmentation rules files are in this repo.

** two-buffer scrolling/sync

- we want this to be both rigorous and flexible. it has been done various times but it has never been done rigorously and flexibly (including  assuming that buffers will lose sync and so making easy/fast ways to recover)
- it should work with all the various ways people scroll and move point, including =cua-mode=, =centered-cursor-mode=, =org-mode=, and anything else that modifies common scrolling commands.

* thoughts about building new tools

martianh: I have been thinking recently about what would be needed to have some decent translation functionality in emacs, and quickly came to the conclusion that developing a CAT tool is likely too much work for an individual working in a free software environment. 

That said, there are various handy packages available. My thought for the moment was that if we agreed on a few common needs, we could each work on implementing them as separate modules, then see about integrating them as minor modes in an encompassing major mode, or similar. Someone could work on segementation, someone on glossaries, somone on UI/highlighting, and so on.

I also think that, at least for my needs, the main things that would be a big help are actually the pretty basic things about highlighting, easy paired-buffer navigation, and so on. Rather than full-fledged CAT tools like glossaries and text analysis.

Do you have some more ideas yourself? Why not let's discuss some and maybe delegate a few simple tasks each?

* non-emacs free software translation resources

** omegat

https://omegat.org/

- free translation memory tool
- fully featured (translation memories, segmentation, glossaries, document conversion/compatability, interface to machine translation)

** apertium

https://apertium.org/index.eng.html#?dir=fra-epo&q=

- GPL, public API
- limited language pairs
- rule-based (not neural)

** okapi framework

https://www.okapiframework.org/

- vast open framework for various translation tasks.
- maybe we could write some basic interfaces to the various tools it includes

** swordfish and friends

https://www.maxprograms.com/products/index.html 

- newer
- is it open/free software? sort of, $120 per year after trial period. but open source.
- good support from the dev
- uses diff gloss format

* other discussion

https://groups.google.com/g/gnu.emacs.help/c/Eb95WZ4iD6g

* other emacs writing resources

** settings from 'emacs for writers' by Jay Dixit

https://github.com/incandescentman/Emacs-Settings

** beautifying org mode

https://zzamboni.org/post/beautifying-org-mode-in-emacs/

** emacs as a word processor

http://www.howardism.org/Technical/Emacs/orgmode-wordprocessor.html

** reddit post

https://www.reddit.com/r/emacs/comments/2wqgyt/need_your_help_in_compiling_a_list_of_packages/

** creative writing with emacs

https://jacmoes.wordpress.com/2019/09/24/creative-writing-with-emacs/

